﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OneMore
{
    [ComVisible(true)]
    public interface IClass1
    {
        int Get10();

        int SubtractNums(int a, int b);
    }

    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class OneMoreTime : IClass1
    {
        public int Get10()
        {
            return 10;
        }

        public int SubtractNums(int a, int b)
        {
            if (b > a)
            {
                throw new ArgumentException("We don't do Negatives!!", new ArithmeticException("That's beyond zeros"));
            } else
            {
                return a - b;
            }
        }
    }
}
