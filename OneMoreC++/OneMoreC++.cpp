#include <iostream>
#include "tchar.h"
/*
* import TLBs. Include tlh if there are errors with the tlb
*/

#import "C:\Users\alis22\ExceptionHandling\OneMore\bin\Debug\OneMore.tlb" no_namespace

int _tmain(int argc, _TCHAR* argv[])
{
    int a, b;
 
    std::cout << "Enter Two Numbers:" << std::endl;
    std::cin >> a;
    std::cin >> b;

    CoInitialize(NULL);

    IClass1Ptr obj1(__uuidof(OneMoreTime));

    try {
        printf("this is your answer: %d", obj1->SubtractNums(a, b));
    }
    catch (_com_error &e) {
        std::cout << "e.Error(), This is the HRESULT, use error lookup to find what the number means: " << e.Error() << std::endl;
        std::cout << "e.Description(): " << e.Description() << std::endl;
        std::cout << "e.Source(): " << e.Source() << std::endl;
    }

    CoUninitialize();
    return 0;
}
